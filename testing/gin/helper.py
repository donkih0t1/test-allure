class Url:
    BASE_URL = "https://petstore.swagger.io/v2"



class userrs:
    def users(self):
        return "/user"

    def user_username(self, user_name):
        return f"{self.users()}/{user_name}"

user = userrs()

class pets:
    def petts(self):
        return "/pet"

    def pet_find_by_status_available(self):
        return f"{self.petts()}/findByStatus?status=available"

    def pet_delete(self, id):
        return f"{self.petts()}/{id}"

    def pet_id(self, id):
        return f"{self.petts()}/{id}"

class store:
    def order(self):
        return "/store"

    def create_order(self):
        return f"{self.order()}/order"

    def get_order(self, id):
        return f"{self.create_order()}/{id}"

    def inventory(self):
        return f"{self.order()}/inventory"