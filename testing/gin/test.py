import requests
import random
import allure
from helper import Url, user, store, pets


@allure.feature("Sozdanie usera")
@allure.story('Создание пользователя')
def testCreatedUser(create_new_user):
    response_user = requests.get(url=f'{Url.BASE_URL}{user.user_username(create_new_user["username"])}')
    with allure.step("smotrim updatenulos ili net"):
        assert response_user.status_code == 200, "Can't find the user"
    with allure.step("smotrim updatenulos ili net"):
        assert create_new_user == response_user.json(), "incorrect user data"
    print(create_new_user)
    print(response_user.json())


@allure.feature("Sozdanie usera")
@allure.story('Улучшение животного')
def test_update_pet(create_new_pet):
    create_new_pet["status"] = "pending"
    inventory = requests.get(url=f"{Url.BASE_URL}{store().inventory()}")
    updated = requests.put(url=f"{Url.BASE_URL}{pets().petts()}", json=create_new_pet)
    with allure.step("smotrim updatenulos ili net"):
        assert create_new_pet == updated.json(), "Pet hasn't updated"
    inventory_up = requests.get(url=f"{Url.BASE_URL}{store().inventory()}")
    print(inventory_up.json())
    with allure.step("smotrim updatenulos ili net"):
        assert int(inventory_up.json()["pending"]) - int(inventory.json()["pending"]) == 1, "Sosati"


@allure.feature("Sozdanie usera")
@allure.story('Тестовый заказ')
def test_order(create_new_pet):
    order = {
        "id": random.randint(1, 10),
        "petId": create_new_pet["id"],
        "quantity": random.randint(1, 20),
        "shipDate": "2021-03-26T12:09:30.638Z",
        "status": "placed",
        "complete": "true"
    }
    create_order = requests.post(url=f'{Url.BASE_URL}{store().create_order()}', json=order)
    with allure.step("smotrim updatenulos ili net"):
        assert create_order.status_code == 200, "Service isn't working"
    get_order = requests.get(url=f'{Url.BASE_URL}{store().get_order(order["id"])}')
    with allure.step(print(order, get_order.json())):
        assert get_order.status_code == 200, "Order is not in the system"
    print(get_order.json())
    print(create_order.json())
