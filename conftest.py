from datetime import datetime
from testing.gin.helper import Url, user, pets
import pytest
import requests
import allure


class ApiClient:
    def __init__(self, base_address):
        self.base_address = base_address

    def post(self, path="/", params=None, data=None, json=None, headers=None):
        url = f"{self.base_address}{path}"
        with allure.step(f'POST request to: {url}'):
            return requests.post(url=url, params=params, data=data, json=json, headers=headers)

    def get(self, path="/", params=None, headers=None):
        url = f"{self.base_address}{path}"
        with allure.step(f'GET request to: {url}'):
            return requests.get(url=url, params=params, headers=headers)

@pytest.fixture
def dog_api():
    return ApiClient(base_address="https://dog.ceo/api/")


@pytest.fixture(scope="function", autouse=False)
def create_new_user():
    new_user = {
        "id": int(f"{datetime.now().timestamp()}"[:-7]) ,
        "username": f"donkih0t{datetime.now().timestamp()}"[:-7],
        "firstName": "Alex",
        "lastName": "Yevdokymov",
        "email": "string",
        "password": "string",
        "phone": "string",
        "userStatus": 0
    }
    c = requests.post(url=f'{Url.BASE_URL}{user.users()}', json=new_user)
    assert c.status_code == 200, "User hasn't created"
    return new_user


@pytest.fixture(scope="function", autouse=True)
def create_new_pet():
    new_pet = {
            "id": int(f"{datetime.now().timestamp()}"[:-7]),
            "category": {
                "id": 0,
                "name": "string"
            },
            "name": "doggie",
            "photoUrls": [
                "string"
            ],
            "tags": [
                {
                    "id": 0,
                    "name": "string"
                }
            ],
            "status": "available"
        }
    c = requests.post(url=f'{Url.BASE_URL}{pets().petts()}', json=new_pet)
    assert c.status_code == 200, "Pet hasn't created"
    return new_pet

